# NOT ON TIME

Bot for automatically fill timesheet on IDONTIME.

When run, it will create an entry on IDONTIME with a 9 to 18 schedule on the desired day.


## Installation

 - Make sure you have Python >= 3.7
 - Run `pip3 install -r requirements.txt`
 - Run `python3 -m playwright install`
 - Create enviroment variables `IDONTIME_USER` and `IDONTIME_PASSWORD`
 - Done :)

## Usage

 - Post
    - For the current day:
      - `notontime.py post`
    - For specific day:
      - `notontime.py post dd-mm-yyyy`
   
 - List
    - For the current month
      - `notontime.py list`
    - For specific month
      - `notontime.py list -y YYYY -m MM`