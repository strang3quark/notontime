from idontime.core.singleton import singleton
from playwright import sync_playwright

import os


@singleton
class Context:

    _playwright = None
    _browser = None
    _page = None

    _username = None
    _password = None

    def __init__(self) -> None:
        super().__init__()
        self._playwright = sync_playwright().start()
        self._browser = self.playwright().chromium.launch(headless=True)
        self._page = self.browser().newPage()

        self._username = os.environ["IDONTIME_USER"]
        self._password = os.environ['IDONTIME_PASSWORD']

    def playwright(self):
        return self._playwright

    def browser(self):
        return self._browser

    def page(self):
        return self._page

    def username(self):
        return self._username

    def password(self):
        return self._password
