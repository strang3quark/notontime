# login
url = "https://idontime.vwgs.pt/default.aspx?l=1"
input_username = "cpLogin_txtUtilizador_I"
btn_login = "cpLogin_btnEntrar_CD"

# add movements
url_movements = "https://idontime.vwgs.pt/areas/as/asmovimentos.aspx"
btn_add = "ctl00_ASPxSplitter_botoes_btnAdicionar"
frame_add = "ctl00_ASPxSplitter_cphContent_popupEdita_CIF-1"
input_date = "ctl00_cphContent_txtData_I"
btn_submit = "ctl00_btnGuardar"

# list movements
input_date_begin = "ctl00_ASPxSplitter_cphLateral_cpDatas_txtDataInicio_I"
input_date_end = "ctl00_ASPxSplitter_cphLateral_cpDatas_txtDataFim_I"
btn_refresh_list = "ctl00_ASPxSplitter_botoes_btnRefrescar"
table_movements = "ctl00_ASPxSplitter_cphContent_grid_DXMainTable"
dropdown_table_size = "ctl00_ASPxSplitter_cphContent_grid_DXPagerBottom_PSB"
