from idontime import constant
from idontime.context import Context


def do_login():
    ctx = Context()

    page = ctx.page()

    page.goto(constant.url)
    page.fill("id={}".format(constant.input_username), ctx.username())
    page.keyboard.press("Tab")
    page.keyboard.type(ctx.password())
    page.click("id={}".format(constant.btn_login))
