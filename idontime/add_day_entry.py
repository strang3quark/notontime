from idontime import constant
from idontime.context import Context


def add_day(date, clock_in, clock_out):
    add_entry(date, clock_in)
    add_entry(date, clock_out)


def add_entry(date, time):
    ctx = Context()

    page = ctx.page()

    page.goto(constant.url_movements)
    page.waitForTimeout(1000)

    page.click("id={}".format(constant.btn_add))
    page.waitForTimeout(1000)

    frame = page.frame(constant.frame_add)
    frame.fill("id={}".format(constant.input_date), "{} {}".format(date, time))

    frame.click("id={}".format(constant.btn_submit))
