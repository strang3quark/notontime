import calendar
from idontime import constant
from idontime.context import Context
from lxml import etree


def get_month_entries(year, month):
    ctx = Context()

    page = ctx.page()

    page.goto(constant.url_movements)
    page.waitForTimeout(1000)

    last_day = calendar.monthrange(int(year), int(month))[1]

    page.fill("id={}".format(constant.input_date_begin), "{}-{}-{}".format("01", month, year))
    page.fill("id={}".format(constant.input_date_end), "{}-{}-{} 23:59".format(last_day, month, year))
    page.click("id={}".format(constant.dropdown_table_size))
    page.waitForTimeout(500)
    page.keyboard.press("ArrowDown")
    page.keyboard.press("ArrowDown")
    page.keyboard.press("ArrowDown")
    page.keyboard.press("Enter")

    page.waitForTimeout(1000)

    elem = page.querySelector("id={}".format(constant.table_movements))
    return entries_from_table_html(elem.innerHTML())


def entries_from_table_html(html):
    entries = []

    table = etree.HTML(html).find("body/tbody")
    rows = iter(table)
    next(rows)
    for row in rows:
        values = [col.text for col in row]
        day = values[0]
        hour = values[1]
        mov = values[7]
        entries.append([day, hour, mov])

    return entries
