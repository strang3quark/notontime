#!/usr/bin/env python3

from datetime import datetime
from time import strftime

import argparse

import config
from idontime import login, add_day_entry, get_month_entries
from idontime.context import Context


class NotOnTime:
    ctx_idontime = None

    def __init__(self) -> None:
        super().__init__()

        self.ctx_idontime = Context()
        login.do_login()

    @staticmethod
    def clock(date_str):
        if not date_str:
            date_str = NotOnTime.today_date_as_str()
        add_day_entry.add_day(date_str, config.clock_in, config.clock_out)

    @staticmethod
    def list(year, month):
        if year is None:
            year = str(datetime.now().year)

        if month is None:
            month = str(datetime.now().month)

        lst = get_month_entries.get_month_entries(year, month)
        lst.reverse()
        for row in lst:
            print("{} | {} | {}".format(row[0], row[1], row[2]))

    @staticmethod
    def today_date_as_str():
        return strftime("%d-%m-%Y")

    def close(self):
        self.ctx_idontime.browser().close()


if __name__ == '__main__':
    main = None
    print("NOT ON TIME (The rogue bot)")

    parser = argparse.ArgumentParser()

    parser.add_argument("action", help="action to execute, possible actions: post, list")

    clock_group = parser.add_argument_group(title='post action')
    clock_group.add_argument("-d", "--date", help="date to clock in and clock out")

    list_group = parser.add_argument_group(title='list action')
    list_group.add_argument("-y", "--year")
    list_group.add_argument("-m", "--month")

    args = parser.parse_args()

    if args.action is not None:
        main = NotOnTime()

    if args.action == "post":
        date = args.date
        main.clock(date)

    if args.action == "list":
        main.list(args.year, args.month)

    if main is not None:
        main.close()
